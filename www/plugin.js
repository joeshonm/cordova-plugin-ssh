'use strict';

var exec = require('cordova/exec');

var PLUGIN_NAME = 'SSH';


var SSH = {
	connect: function (username, password, ip, port, success, failure) {
		exec(success, failure, PLUGIN_NAME, 'connect', [username, password, ip, port]);
	},
	disconnect: function (cb) {
		exec(cb, null, PLUGIN_NAME, 'disconnect', []);
	},
	getSignal: function (success, failure) {
		exec(success, failure, PLUGIN_NAME, 'getSignal', []);
	}
};

module.exports = SSH;
