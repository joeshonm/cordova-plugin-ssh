package com.joeshonm;


import org.apache.commons.net.telnet.EchoOptionHandler;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.TerminalTypeOptionHandler;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.util.Log;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.telnet.TelnetClient;


public class SSH extends CordovaPlugin {
    private static final String TAG = "SSH";
    private TelnetClient telnetClient;
    private InputStream inputStream;
    private OutputStream outputStream;
    private Double rxPower = -40.0;
    private Double rxSnr = -10.0;
    private String rxPowerRegex = "Rx Power:\\s(-?\\d*\\.{0,1}\\d+)";
    private String rxSnrRegex = "Rx SNR:\\s(-?\\d*\\.{0,1}\\d+)";
    private Pattern rxPowerPattern = Pattern.compile(rxPowerRegex);
    private Pattern rxSnrPattern = Pattern.compile(rxSnrRegex);
    private Boolean isConnectedToNetwork;
    private Boolean isConnectedToSocket = false;
    private Boolean isAuthorized = true;
    private String socketError = "";
    private String socketErrorType = "";

    // credentials
    private String username = "";
    private String password ="";
    private String ip = "";
    private String port = "";


    private static Context context;
    private static ConnectivityManager connectivityManager;

    class AccessDeniedException extends IOException
    {
        private String type;

        //Parameterless Constructor
        public AccessDeniedException() {}

        //Constructor that accepts a message
        public AccessDeniedException(String message, String type)
        {
            super(message);

            this.type = type;
        }

        public String getType() {
            return this.type;
        }


    }

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.context = this.cordova.getActivity().getWindow().getContext();
        this.connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        Log.d(TAG, "Initializing SSH");


    }

    public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("connect")) {
            this.username = args.getString(0);
            this.password = args.getString(1);
            this.ip = args.getString(2);
            this.port = args.getString(3);


            // Return parameters
            JSONObject obj = new JSONObject();
            obj.put("username", this.username);
            obj.put("password", this.password);
            obj.put("ip", this.ip);
            obj.put("port", this.port);

            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            isConnectedToNetwork = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnectedToNetwork) {
                Log.d("Network", "Network Connected");
                Log.d("Network", "Network Type: "+activeNetwork.getTypeName());
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(activeNetwork.getTypeName()).setTitle("Network Type");
                AlertDialog dialog = builder.create();
                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                //dialog.show();

                new TelnetTask().execute(this.username, this.password, this.ip, this.port);

                final PluginResult result = new PluginResult(PluginResult.Status.OK, true);
                callbackContext.sendPluginResult(result);

            } else {

                JSONObject errorObj = new JSONObject();
                errorObj.put("type", "Network Error");
                errorObj.put("message", "There was an error connecting to the modem. Please check cable connection.");

                final PluginResult result = new PluginResult(PluginResult.Status.ERROR, errorObj);
                callbackContext.sendPluginResult(result);
            }






            //new SSHTask().execute("su -c", "su");

            // Set Ethernet IP address


//            System.setProperty("net.eth0.ip", "192.168.0.2");
//
//            System.setProperty("net.eth0.route", "192.168.0.1");
//
//            System.setProperty("net.eth0.dns1", "4.2.2.1");



        } else if (action.equals("disconnect")) {
            // An example of returning data back to the web layer

            try {
                if (telnetClient.isConnected()) {
                    telnetClient.disconnect();

                    final PluginResult result = new PluginResult(PluginResult.Status.OK, true);
                    callbackContext.sendPluginResult(result);
                }
            } catch (IOException e) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(e.getMessage()).setTitle("Telnet Error");
                AlertDialog dialog = builder.create();
                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                //dialog.show();

                final PluginResult result = new PluginResult(PluginResult.Status.ERROR, false);
                callbackContext.sendPluginResult(result);
            }
        } else if (action.equals("getSignal")) {
            LOG.d(TAG, "GET SIGNAL");

            if (telnetClient.isConnected()) {

                JSONObject obj = new JSONObject();
                obj.put("rxpower", rxPower);
                obj.put("rxsnr", rxSnr);


                Log.d(TAG, rxPower.toString());


                final PluginResult result = new PluginResult(PluginResult.Status.OK, obj.toString());
                callbackContext.sendPluginResult(result);


            } else {

                if (!isConnectedToSocket) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", socketErrorType);
                    obj.put("message", socketError);

                    final PluginResult result = new PluginResult(Status.ERROR, obj);
                    callbackContext.sendPluginResult(result);
                } else {
                    JSONObject obj = new JSONObject();
                    obj.put("type", socketErrorType);
                    obj.put("message", socketError);

                    final PluginResult result = new PluginResult(Status.ERROR, obj);
                    callbackContext.sendPluginResult(result);
                }

                new TelnetTask().execute(this.username, this.password, this.ip, this.port);

            }
        }
        return true;
    }

    private void onSSHTaskComplete(String step) {

        if (step == "su") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new SSHTask().execute("su -c netcfg", "netcfg");
        }


        if (step == "netcfg") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new SSHTask().execute("su -c ifconfig eth0 192.168.0.2 netmask 255.255.255.0", "eth0");
        }

        if (step == "eth0") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new SSHTask().execute("su -c route add default gw 192.168.0.1 dev eth0", "route");
        }

        if (step == "eth0_failed") {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Please connect ethernet adapter.").setTitle("Error");
            AlertDialog dialog = builder.create();
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

        if (step == "route") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new SSHTask().execute("su -c setprop net.eth0.dns1 4.2.2.1", "setprop");
        }

        if (step == "setprop") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new SSHTask().execute("su -c netcfg", "netcfg2");
        }

        if (step == "netcfg2") {
            Log.d(TAG, "GOT: "+step);
            SystemClock.sleep(2000);
            new TelnetTask().execute("su -c telnet 192.168.0.1", "telnet");
            //new TelnetTask().execute("192.168.0.1", "23");


            try {
                StringBuffer data = new StringBuffer(1000);
                String filePath = "/sys/class/net/eth0/address";
                BufferedReader reader = new BufferedReader(new FileReader(filePath));
                char[] buf = new char[1024];
                int numRead=0;
                while((numRead=reader.read(buf)) != -1){
                    String readData = String.valueOf(buf, 0, numRead);
                    data.append(readData);
                }
                reader.close();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(data.toString()).setTitle("Ethernet");
                AlertDialog dialog = builder.create();
                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            } catch (IOException e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(e.getMessage()).setTitle("Ethernet");
                AlertDialog dialog = builder.create();
                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }




        }

        if (step == "telnet") {
            Log.d(TAG, "GOT: "+step);
        }
    }

    private class SSHTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... params) {

            String command = params[0];
            String step = params[1];

            try {

                Runtime r = Runtime.getRuntime();
                Process p = r.exec(command);
                p.waitFor();
                BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                String output = "";

                while ((line = b.readLine()) != null) {
                    Log.d(TAG, line);
                    output += " "+line;
                }

                if (step == "netcfg2") {
                    Log.d(TAG, output);
                    CharSequence cs1 = "eth0";
                    boolean retval = output.contains(cs1);

                    if (!retval) {
                        //step = "eth0_failed";
                    }
                }


                b.close();

            } catch(Exception e) {

                Log.d(TAG, e.getMessage());
            }


            return step;


        }

        @Override
        protected void onPostExecute(String result) {

            onSSHTaskComplete(result);
        }
    }

    private class TelnetTask extends AsyncTask<String, Void, String>  {

        private String username = "";
        private String password = "";
        private String ip = "";
        private String port = "";

        protected String doInBackground(String... params) {


            this.username = params[0];
            this.password = params[1];
            this.ip = params[2];
            this.port = params[3];

            String result = "";

            LOG.d(TAG, "Telnet Transport trying to connect...");

            // Create a new telnet client
            telnetClient = new TelnetClient();

            // VT100 terminal type will be subnegotiated
            TerminalTypeOptionHandler ttopt = new TerminalTypeOptionHandler("VT100", false, false, true, false);
            // WILL SUPPRESS-GA, DO SUPPRESS-GA options
            SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);
            // WON'T ECHO, DON'T ECHO
            EchoOptionHandler echoopt = new EchoOptionHandler();

            try {
                // set telnet client options
                telnetClient.addOptionHandler(ttopt);
                telnetClient.addOptionHandler(gaopt);
                telnetClient.addOptionHandler(echoopt);


                // connect
                telnetClient.connect(ip, Integer.parseInt(port));

                isConnectedToSocket = true;
                Boolean isAuthenticated = false;

                inputStream = telnetClient.getInputStream();
                outputStream = telnetClient.getOutputStream();

                try
                {
                    byte[] buff = new byte[1024];
                    int ret_read = 0;

                    do
                    {
                        ret_read = inputStream.read(buff);
                        if(ret_read > 0) {
                            Log.d("Telnet", new String(buff, 0, ret_read));
                            result = new String(buff, 0, ret_read);

                            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outputStream));

                            if (result.contains("Username:")) {
                                out.write(username);
                                out.newLine();
                                out.flush();
                            }

                            if (result.contains("Password:")) {
                                out.write(password);
                                out.newLine();
                                out.write("xoff");
                                out.newLine();
                                out.write("rx snr");
                                out.newLine();
                                out.flush();
                                out.write("rx power");
                                out.newLine();
                                out.flush();

                                isAuthenticated = true;

                            }

                            if (result.contains("Rx SNR:")) {
                                Matcher m = rxSnrPattern.matcher(result);
                                if (m.find()) {
                                    Log.d("RX SNR", m.group(1));
                                    rxSnr = Double.parseDouble(m.group(1));
                                }
                            }

                            if (isAuthenticated) {
                                out.write("rx snr");
                                out.newLine();
                                out.flush();
                            }

                            if (result.contains("Rx Power:")) {
                                Matcher m = rxPowerPattern.matcher(result);
                                if (m.find()) {
                                    Log.d("RX Power", m.group(1));
                                    rxPower = Double.parseDouble(m.group(1));
                                }
                            }

                            if (isAuthenticated) {
                                out.write("rx power");
                                out.newLine();
                                out.flush();
                            }


                            if (result.contains("Access Denied")) {
                                socketErrorType = "Access Denied";
                                socketError = "Please check your modem login credentials and try again.";
                                throw new AccessDeniedException(socketError, socketErrorType);
                            }

                        } else {
                            telnetClient.disconnect();
                        }
                    }
                    while (ret_read >= 0);
                }
                catch (AccessDeniedException e) {
                    Log.d("Access Denied", e.getMessage());
                    socketErrorType = e.getType();
                    socketError = e.getMessage();
                    result = socketError;
                    isAuthorized = false;
                    telnetClient.disconnect();
                }
                catch (IOException e)
                {
                    Log.d("Telnet Error","Exception while reading socket:" + e.getMessage());
                    result = e.getMessage();
                    socketErrorType = "Socket Error";
                    socketError = result;
                }

            } catch (Exception e) {
                Log.d("Telnet Connection Error", e.getMessage());
                result = e.getMessage();
                socketErrorType = "Socket Error";
                socketError = result;
            }


            return result;


        }

        @Override
        protected void onPostExecute(String result) {

            if (result == "Socket Closed") {
                isConnectedToSocket = false;
            }

        }
    }


}