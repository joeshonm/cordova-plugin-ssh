# Cordova Plugin SSH

Establishing an SSH connection and running commands from your Cordova application.

## Getting Started

### Prerequisites

- Cordova 6+


### Installing

```
cordova plugin add cordova-plugin-device
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **JoeShon Monroe**  - [X Studios, Inc](http://xstudios.agency)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
